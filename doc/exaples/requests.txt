GET http://localhost:8080/api/product?id=1

GET http://localhost:8080/api/product/all


POST http://localhost:8080/api/admin/product

{
    "id": 3,
    "name": "Młotek",
    "price": 25.0,
    "available": false
}


PUT http://localhost:8080/api/admin/product?id=2
{
    "id": 2,
    "name": "Kołek",
    "price": 2.0,
    "available": false
}
PATCH http://localhost:8080/api/admin/product?id=2
{
    "id": 2,
    "name": "Kołek",
    "price": 4.0,
    "available": false
}

GET http://localhost:8080/api/customer?id=2

GET http://localhost:8080/api/customer/all


POST http://localhost:8080/api/admin/customer
{
    "id": 3,
    "name": "Tomasz Foka",
    "address": "Gdańsk"
}

PUT http://localhost:8080/api/admin/customer?id=2
Body:
{
        "id": 2,
        "name": "Mariola Kowalczyk",
        "address": "Wrocław"
    }

PATCH http://localhost:8080/api/admin/customer?id=3
{
        "id": 2,
        "name": "Mariola Kowalczyk",
        "address": "Brzeg"
    }

GET http://localhost:8080/api/order?id=2


GET http://localhost:8080/api/order/all

POST http://localhost:8080/api/order
{
    "id": 3,
    "customer": {
        "id": 2,
        "name": "Karolina Kowalczyk",
        "address": "Warszawa"
    },
    "products": [
        {
            "id": 1,
            "name": "Śruba",
            "price": 1.5,
            "available": true
        },
        {
            "id": 2,
            "name": "Wiertarka",
            "price": 2500.0,
            "available": true
        }
    ],
    "placeDate": "2021-04-21T19:00:59.26365",
    "status": "in progress"
}
PUT http://localhost:8080/api/admin/order?id=1

{
        "id": 1,
        "customer": {
            "id": 1,
            "name": "Tomek Zielony",
            "address": "Lubin"
        },
        "products": [
            {
                "id": 2,
                "name": "Kołek",
                "price": 2.0,
                "available": false
            },
            {
                "id": 1,
                "name": "Śruba",
                "price": 1.5,
                "available": true
            }
        ],
        "placeDate": "2021-04-23T08:00:20.483264",
        "status": "send"
    }
PATCH http://localhost:8080/api/admin/order?id=1
{
        "id": 1,
        "customer": {
            "id": 1,
            "name": "Tomek Zielony",
            "address": "Brzeg Dolny"
        },
        "products": [
            {
                "id": 2,
                "name": "Kołek",
                "price": 2.0,
                "available": false
            },
            {
                "id": 1,
                "name": "Śruba",
                "price": 1.5,
                "available": true
            }
        ],
        "placeDate": "2021-04-23T08:00:20.483264",
        "status": "send"
    }
