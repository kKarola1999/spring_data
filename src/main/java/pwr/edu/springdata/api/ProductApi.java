package pwr.edu.springdata.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import pwr.edu.springdata.dao.entity.Product;
import pwr.edu.springdata.manager.CustomerManager;
import pwr.edu.springdata.manager.ProductManager;

import java.util.Map;
import java.util.Optional;


    @RestController
    @RequestMapping("/api")
    public class ProductApi {
        private ProductManager products;

        @Autowired
        public ProductApi(ProductManager products) {
            this.products = products;
        }


        @GetMapping("/product/all")
        public Iterable<Product> getAll() {
            return products.findAll();

        }

        @GetMapping("/product")
        public Optional<Product> getById(@RequestParam long id) {
            return products.findById(id);

        }
        @PostMapping("/admin/product")
        public Product addProduct(@RequestBody Product product){
            return products.save(product);

        }

        @PutMapping("/admin/product")
        public Product putProduct( @RequestBody Product product){
            return products.save(product);

        }

        @PatchMapping("admin/product/{id}")
        @ResponseStatus(HttpStatus.NO_CONTENT)
        public Product patchProduct(@RequestBody Map<String, Object> updates, @PathVariable("id") Long id) {
            Product product =  products.partialUdpate(id, updates);
            if (products.partialUdpate(id, updates) == null) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Product not found");
            }return product;
        }

        }


