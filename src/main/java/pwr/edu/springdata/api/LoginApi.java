package pwr.edu.springdata.api;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.web.bind.annotation.*;
import pwr.edu.springdata.dao.entity.User;
import pwr.edu.springdata.manager.UserManager;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

@RestController
@RequestMapping("/api")
public class LoginApi {
    private UserManager userManager;


    public LoginApi(UserManager userManager) {
        this.userManager = userManager;

    }

    @PostMapping("/getToken")
    public String get(@RequestBody User user) throws Exception {
        //long currentTimeMillis=System.currentTimeMillis();
        LocalDateTime dateStart=LocalDateTime.now();
        LocalDateTime dateEnd=LocalDateTime.now().plusMinutes(10);





        if (userManager.checkUser(user)) {


            return Jwts.builder()
                    .setSubject(user.getName())
                    .claim("roles", user.getRole())
                    .setIssuedAt(java.util.Date.from(dateStart.atZone(ZoneId.systemDefault()).toInstant()))
                    .setExpiration(java.util.Date.from(dateEnd.atZone(ZoneId.systemDefault()).toInstant()))
                    .signWith(SignatureAlgorithm.HS512, user.getPassword())
                    .setHeaderParam("typ","JWT")
                    .compact();
        }
        else{
            throw new Exception("User not exist");
            }

    }


}
