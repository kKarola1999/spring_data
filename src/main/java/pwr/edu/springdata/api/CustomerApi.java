package pwr.edu.springdata.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import pwr.edu.springdata.dao.entity.Customer;
import pwr.edu.springdata.dao.entity.Order;
import pwr.edu.springdata.manager.CustomerManager;

import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class CustomerApi {
    private CustomerManager customers;

    @Autowired
    public CustomerApi(CustomerManager customers) {
        this.customers = customers;
    }


    @GetMapping("/customer/all")
    public Iterable<Customer> getAll() {
        return customers.findAll();

    }

    @GetMapping("/customer")
    public Optional<Customer> getById(@RequestParam long id) {
        return customers.findById(id);

    }
    @PostMapping("/admin/customer")
    public Customer addCustomer(@RequestBody Customer customer){
        return customers.save(customer);

    }

    @PutMapping("/admin/customer")
    public Customer putCustomer(@RequestBody Customer customer){
        return customers.save(customer);

    }

    @PatchMapping("admin/customer/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public Customer patchCustomer(@RequestBody Map<String, Object> updates, @PathVariable("id") Long id) {
        Customer customer =  customers.partialUdpate(id, updates);
        if (customers.partialUdpate(id, updates) == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Customer not found");
        }return customer;
    }
}
