package pwr.edu.springdata.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import pwr.edu.springdata.dao.entity.Order;
import pwr.edu.springdata.dao.entity.Product;
import pwr.edu.springdata.manager.OrderManager;

import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class OrderApi {

    private OrderManager orders;
    @Autowired
    public OrderApi(OrderManager orders) {
        this.orders = orders;
    }

    @GetMapping("/order/all")
    public Iterable<Order> getAll() {
        return orders.findAll();

    }

    @GetMapping("/order")
    public Optional<Order> getById(@RequestParam long id) {
        return orders.findById(id);

    }

    @PostMapping("/order")
    public Order addOrder(@RequestBody Order order) {
        return orders.save(order);
    }

    @PutMapping("/admin/order")
    public Order putOrder(@RequestBody Order order) {
        return orders.save(order);

    }

    @PatchMapping("admin/order/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public Order patchOrder(@RequestBody Map<String, Object> updates, @PathVariable("id") Long id) {
        Order order =  orders.partialUdpate(id, updates);
        if (orders.partialUdpate(id, updates) == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Order not found");
        }return order;
    }
}


