package pwr.edu.springdata.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pwr.edu.springdata.dao.entity.Order;

@Repository
public interface OrderRepository extends CrudRepository<Order,Long> {
}
