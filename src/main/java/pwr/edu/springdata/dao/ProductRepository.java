package pwr.edu.springdata.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pwr.edu.springdata.dao.entity.Product;

@Repository
public interface ProductRepository extends CrudRepository<Product,Long> {

}
