package pwr.edu.springdata.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pwr.edu.springdata.dao.entity.Customer;

@Repository
public interface CustomerRepository extends CrudRepository<Customer,Long> {
}
