package pwr.edu.springdata.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pwr.edu.springdata.userDto.UserDto;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<UserDto,String> {
     Optional<UserDto> findByName(String name);
}
