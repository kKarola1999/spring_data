package pwr.edu.springdata;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import pwr.edu.springdata.dao.CustomerRepository;
import pwr.edu.springdata.dao.OrderRepository;
import pwr.edu.springdata.dao.ProductRepository;
import pwr.edu.springdata.dao.entity.Customer;
import pwr.edu.springdata.dao.entity.Order;
import pwr.edu.springdata.dao.entity.Product;
import pwr.edu.springdata.dao.entity.User;
import pwr.edu.springdata.manager.UserManager;
import pwr.edu.springdata.userDto.UserDtoBuilder;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Component
public class DbMockData {
    private ProductRepository productRepository;
    private OrderRepository orderRepository;
    private CustomerRepository customerRepository;
    private UserManager userManager;
    private UserDtoBuilder userDtoBuilder;

    @Autowired
    public DbMockData(ProductRepository productRepository, OrderRepository orderRepository, CustomerRepository customerRepository,UserManager userManager, UserDtoBuilder userDtoBuilder) {
        this.productRepository = productRepository;
        this.orderRepository = orderRepository;
        this.customerRepository = customerRepository;
        this.userManager=userManager;
        this.userDtoBuilder=userDtoBuilder;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void fill() {
        Product product = new Product("Śruba", 1.50f, true);
        Product product1 = new Product("Wiertarka", 2500f, true);
       Customer customer = new Customer("Tomek Kowalski", "Wrocław");
        Customer customer1 = new Customer("Karolina Kowalczyk", "Warszawa");
        Set<Product> products = new HashSet<>() {
            {
                add(product);
                add(product1);
            }};

        Order  order = new Order(customer,products, LocalDateTime.now(), "in progress");
        Order order1 = new Order(customer1, products, LocalDateTime.now(), "send");

        productRepository.save(product);
        productRepository.save(product1);
        customerRepository.save(customer);
        customerRepository.save(customer1);
        orderRepository.save(order);
        orderRepository.save(order1);
        User admin=new User("apiadmin","adminPassword", "ROLE_ADMIN");
        User user=new User("apiuser","userPassword", "ROLE_CUSTOMER");
        userManager.save(userDtoBuilder.createUser(admin));
        userManager.save(userDtoBuilder.createUser(user));



    }
}