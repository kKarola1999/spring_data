package pwr.edu.springdata.userDto;


import org.springframework.stereotype.Component;
import pwr.edu.springdata.PasswordEncoderConfig;
import pwr.edu.springdata.dao.entity.User;


@Component
public class UserDtoBuilder {

        private PasswordEncoderConfig passwordEncoderConfig;


        public UserDtoBuilder(PasswordEncoderConfig passwordEncoderConfig) {

            this.passwordEncoderConfig = passwordEncoderConfig;
        }


        public UserDto createUser(User user) {
            return new UserDto(user.getName(), passwordEncoderConfig.passwordEncoder().encode(user.getPassword()), user.getRole());

        }
        }


