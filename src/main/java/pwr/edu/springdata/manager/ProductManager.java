package pwr.edu.springdata.manager;

import org.springframework.stereotype.Service;
import pwr.edu.springdata.dao.CustomerRepository;
import pwr.edu.springdata.dao.ProductRepository;
import pwr.edu.springdata.dao.entity.Customer;
import pwr.edu.springdata.dao.entity.Product;

import java.util.Map;
import java.util.Optional;
@Service
public class ProductManager {

    private ProductRepository productRepository;

    public ProductManager(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }
    public Optional<Product> findById(Long id){
        return productRepository.findById(id);
    }
    public Iterable<Product> findAll(){
        return productRepository.findAll();
    }
    public Product save(Product product){
        return productRepository.save(product);
    }
    public Product partialUdpate(Long id, Map<String, Object> updates){
        Product product =  findById(id).get();

        if(updates.containsKey("name"))
        {
            product.setName(updates.get("name").toString());
        }
        if (updates.containsKey("price")){
            product.setPrice(Float.parseFloat(updates.get("placeDate").toString()));
        }
        if (updates.containsKey("available")){
            product.setAvailable(Boolean.parseBoolean(updates.get("available").toString()));
        }
        return productRepository.save(product);

    }

    public void fillDB(){
        save(new Product("Śruba", 1.50f, true));
        save(new Product("Wiertarka", 2500f, true));

    }
}
