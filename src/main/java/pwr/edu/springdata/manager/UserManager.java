package pwr.edu.springdata.manager;

import org.springframework.stereotype.Service;
import pwr.edu.springdata.PasswordEncoderConfig;
import pwr.edu.springdata.userDto.UserDto;
import pwr.edu.springdata.dao.UserRepository;
import pwr.edu.springdata.dao.entity.User;

import java.util.Optional;
@Service
public class UserManager {
    private UserRepository userRepository;

    private PasswordEncoderConfig passwordEncoderConfig;



    public UserManager(UserRepository userRepository, PasswordEncoderConfig passwordEncoderConfig) {
        this.userRepository = userRepository;
        this.passwordEncoderConfig=passwordEncoderConfig;



    }
    public Optional<UserDto> findByName(String name){
        return userRepository.findByName(name);
    }

    public UserDto save(UserDto user){
        return userRepository.save(user);
    }

    public boolean checkUser(User user){
        boolean exist = false;

        if (findByName(user.getName()).isPresent())
        {
            UserDto userDto =  findByName(user.getName()).get();
            boolean encoded =  passwordEncoderConfig.passwordEncoder().matches(user.getPassword(), userDto.getPasswordHash());
            if (encoded==true)
            {
                exist = true;
            }
        }
        return exist;
    }

    public void fillDB(){






    }

}
