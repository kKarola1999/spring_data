package pwr.edu.springdata.manager;

import org.hibernate.sql.Update;
import org.springframework.stereotype.Service;
import pwr.edu.springdata.dao.OrderRepository;
import pwr.edu.springdata.dao.entity.Customer;
import pwr.edu.springdata.dao.entity.Order;
import pwr.edu.springdata.dao.entity.Product;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@Service
public class OrderManager {

    private OrderRepository orderRepository;

    public OrderManager(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }
    public Optional<Order> findById(Long id){
        return orderRepository.findById(id);
    }
    public Iterable<Order> findAll(){
        return orderRepository.findAll();
    }
    public Order save(Order order){
        return orderRepository.save(order);
    }

    public Order partialUdpate(Long id, Map<String, Object> updates){
        Order order =  findById(id).get();

        if(updates.containsKey("customer"))
        {
            order.setCustomer((Customer) updates.get("customer"));
        }
        if (updates.containsKey("products")){
            order.setProducts((Set<Product>) updates.get("products"));
        }
        if (updates.containsKey("placeDate")){
            order.setPlaceDate((LocalDateTime) updates.get("placeDate"));
        }
        if (updates.containsKey("status")){
            order.setStatus(updates.get("status").toString());
        }
        return orderRepository.save(order);

    }

    //public void fillDB(){



    //}
}
