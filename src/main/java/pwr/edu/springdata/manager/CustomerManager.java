package pwr.edu.springdata.manager;

import org.springframework.stereotype.Service;
import pwr.edu.springdata.dao.CustomerRepository;
import pwr.edu.springdata.dao.entity.Customer;
import pwr.edu.springdata.dao.entity.Product;

import java.util.Map;
import java.util.Optional;

@Service
public class CustomerManager {
    private CustomerRepository customerRepository;

    public CustomerManager(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }
    public Optional<Customer> findById(long id){
        return customerRepository.findById(id);
    }
    public Iterable<Customer> findAll(){
        return customerRepository.findAll();
    }
    public Customer save(Customer customer){
        return customerRepository.save(customer);
    }

    public Customer partialUdpate(Long id, Map<String, Object> updates){
        Customer customer =  findById(id).get();

        if(updates.containsKey("name"))
        {
            customer.setName(updates.get("name").toString());
        }
        if (updates.containsKey("address")){
            customer.setAddress(updates.get("address").toString());
        }

        return customerRepository.save(customer);

    }

    public void fillDB(){
        save(new Customer("Tomek Kowalski", "Wrocław"));
        save(new Customer("Karolina Kowalczyk", "Warszawa"));

    }
}
